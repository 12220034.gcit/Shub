// Packages

// Components

// Logic

// Context

// Services

// Styles
import "./Subheadline.css";

// Assets

export const Subheadline = () => {
	return (
		<div className='landing-hero-subheadline'>
			Story Hub is a platform for writers to store and share their stories, read interesting articles everyday, and connect with people around the world.
			<br />
			<br />
			Create stories that you love and wish to tell!
		</div>
	);
};
